List of modifications
---

General
---

- A multitude of miscellaneous warning fixes.
- Players gain frag count for killing monsters. Count persists across levels.
- Keys are kept after use.
- A message is broadcast printed after killing a shambler.
- Zombies can now be gibbed with the axe when they're grounded. Adapted from honey mod.
- Fixed swimmonsters being added to total_monsters twice.
- Various miscellaneous minor fixes.
- Adapted gib velocity behavior from QuakeWorld.
- Added `EF_RED` and `EF_BLUE` effects from QuakeWorld.
- Included other minor changes from QuakeWorld.

COOP specific
---

- Items respawn as they would in deathmatch, with added visual effects.
- Players always start levels at 100 health.
- With teamplay at 1, friendly fire is disabled, including self inflicted, for both armor and health. Teammate momentum is also unaffected when being fired at.
- Key pickups and opening locked doors are broadcasted.
- Rune pickups and finding secret areas are broadcasted.
- Player's keys held are kept after death.
- Players cannot pickup other players' backpacks.
- The axe can be used to heal, up to 150 hp. Heals 2 points per hit for self when hitting wall, and 4 points for both self and teamate when attacking each-other.
- A disposable personal spawn point can be placed using `impulse 42` that the player will spawn at on death instead of a map start point. Player's backpack is also teleported.
Spawn points have a 5 second cooldown and cannot be placed within a 200 unit radius of an existing point.
- The start map is skipped after ending an episode and next episode is started immediately.
- **Revenge system:**  
Monsters now gain frag count for killing players, and keep track of name of recent player killed. Other monster's frag count and recent kill name are absorbed from an infight kill. Players can regain these frags by killing monsters with a frag count for a revenge kill. Revenge kills are broadcasted, including the name of the player avenged if not self.


